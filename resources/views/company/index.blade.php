<!DOCTYPE html>
 
<html lang="en">
<head>
<title>Company list</title>
<meta name="csrf-token" content="{{ csrf_token() }}">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">  
<link  href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.5.2/bootbox.min.js"></script>
</head>
      <body>
         <div class="container">
               <h2>Company list</h2>
               @if(session()->has('success'))
                  <div class="alert alert-success">
                     {{ session()->get('success') }}
                  </div>
               @endif
               @if(session()->has('error'))
                  <div class="alert alert-danger">
                     {{ session()->get('error') }}
                  </div>
               @endif
               <div class="pull-right">
               <a href="{{ route('home') }}" class="btn btn-danger">Back</a>
               <a href="javascript:void(0)" class="btn btn-success mb-2" id="new-customer" data-toggle="modal">New Company</a>
               </div>
            <table class="table table-bordered" id="laravel_datatable">
               <thead>
                  <tr>
                     <th>Id</th>
                     <th>Name</th>
                     <th>Email</th>
                     <th>Mobile</th>
                     <th>Action</th>
                  </tr>
               </thead>
            </table>
            <div class="modal fade" id="crud-modal" aria-hidden="true" >
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title" id="customerCrudModal"></h4>
</div>
<div class="modal-body">
<form name="custForm" id="formId" action="{{ route('company.store') }}" method="POST" enctype="multipart/form-data">
<input type="hidden" name="cust_id" id="cust_id" >
@csrf
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>Name:</strong>
<input type="text" name="name" id="name" class="form-control" placeholder="Name" onchange="validate()" >
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>Email:</strong>
<input type="text" name="email" id="email" class="form-control" placeholder="Email" onchange="validate()">
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>Mobile:</strong>
<input type="text" name="mobile" id="mobile" class="form-control" placeholder="Mobile" onchange="validate()" onkeypress="validate()">
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>logo:</strong>
<input type="file" name="profile_image" id="profile_image"  class="form-control">
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 text-center">
<button type="submit" id="btn-save" name="btnsave" class="btn btn-primary" disabled>Submit</button>
<a href="{{ route('company.index') }}" class="btn btn-danger">Cancel</a>
</div>
</div>
</form>
</div>
</div>
</div>
</div>
         </div>
   <script>
   $(document).ready( function () {
    $('#laravel_datatable').DataTable({
           processing: true,
           serverSide: true,
           ajax: {
                url: '{!! route('company.datatable') !!}',
                type: 'post',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                data: function (d) {
                    
                }
            },
           columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name' },
                    { data: 'email', name: 'email' },
                    { data: 'mobile', name: 'mobile' },
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                 ]
        });
     });
     $('#new-customer').click(function () {
$('#btn-save').val("create-customer");
$('#customer').trigger("reset");
$('#customerCrudModal').html("Add New Customer");
$('#crud-modal').modal('show');
});
/* Delete customer */
$('body').on('click', '#delete-customer', function () {
    var id = $(this).data("id");
        var token = $("meta[name='csrf-token']").attr("content");
    
        $.ajax(
        {
            url: "company/"+id,
            type: 'DELETE',
            data: {
                "id": id,
                "_token": token,
            },
            success: function (data){
               
               bootbox.alert(data.success);
            }
        });
   
});
/* Edit customer */
$('body').on('click', '#edit-customer', function () {
   
      var customer_id = $(this).data('id');
      $.get('company/'+customer_id+'/edit', function (data) {
         console.log(data.data.first_name);
      $('#customerCrudModal').html("Edit company");
      $('#btn-update').val("Update");
      $('#btn-save').prop('disabled',false);
      $('#crud-modal').modal('show');
      $('#cust_id').val(data.data.id);
      $('#name').val(data.data.name);
      $('#email').val(data.data.email);
      $('#mobile').val(data.data.mobile);
      $("#profile_image").hide();
      })
    });

function validate()
{
	if(document.custForm.name.value !='' && document.custForm.email.value !='' && document.custForm.mobile.value !='')
	    document.custForm.btnsave.disabled=false
	else
		document.custForm.btnsave.disabled=true
}
  </script>
   </body>
</html>