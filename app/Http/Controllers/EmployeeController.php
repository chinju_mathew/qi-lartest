<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Datatables;
use DB;
use Validator;
use App\Employees;
use App\Companies;
class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $company = Companies::all();
      
        
        return view('employee.index',compact('company'));
    }

    public function usersList(){
        
        $users = DB::table('employees')->select('*');
        return datatables()->of($users)
        ->editColumn('company_name', function ($users) {
            if (!empty($users->company_name)) {
                $companyname = Companies::where('id', $users->company_name)->first();
                
                return !empty($companyname) ? $companyname->name : '-';
            }
            return '-';
        })
        ->addIndexColumn()
                    ->addColumn('action', function($row) {
                           $btn = '<a href="javascript:void(0)" class="btn btn-success" id="edit-customer" data-toggle="modal" data-id="'.$row->id.'">Edit </a> <a href="javascript:void(0)" id="delete-customer" data-id="'.$row->id.'" class="edit btn btn-primary btn-sm">Delete</a>';
                           return $btn;
                    })
                    ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->btnsave =="create-customer"){
        $validator = Validator::make(
            $request->all(),
            [
            'firstname'     => 'required|alpha',
            'mobile'     => 'required',
            'lastname'     => 'required|alpha',
            'email'     => 'required',
            ]
        );
        if ($validator->fails()) {
            return response()->json($validator->errors(), 406);
        }
        $admin = new Employees();
        $admin->first_name = $request->firstname;
        $admin->last_name = $request->lastname;
        $admin->company_name = $request->companyname;
        $admin->email = $request->email;
        $admin->mobile = $request->mobile;
        $admin->save();
       
			$msg = 'Employee data is created successfully';
		return redirect()->route('employee.index')->with('success',$msg);
    }else{
        $affected = DB::table('employees')
              ->where('id', $request->cust_id)
              ->update(['first_name' => $request->firstname,'last_name' => $request->lastname,'company_name' => $request->companyname,'email' => $request->email,'mobile' => $request->mobile]);
              $msg = 'Employee data is update successfully';
		return redirect()->route('employee.index')->with('success',$msg);
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $where = array('id' => $id);
		$customer = Employees::where($where)->first();
		return response()->json([
            "data" => $customer,]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Employees::find($id)->delete($id);
  
        return response()->json([
            'success' => 'Record deleted successfully!'
        ]);
    }
}
