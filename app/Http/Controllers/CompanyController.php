<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Datatables;
use DB;
use Validator;
use App\Companies;
use App\Employees;
class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('company.index');
    }

    public function usersList(){
        
        $users = DB::table('companies')->select('*');
        return datatables()->of($users)
        ->addIndexColumn()
                    ->addColumn('action', function($row) {
                           $btn = '<a href="javascript:void(0)" class="edit btn btn-primary btn-sm"  id="edit-customer"  data-id="'.$row->id.'">Update</a> <a href="javascript:void(0)" id="delete-customer" data-id="'.$row->id.'" class="edit btn btn-primary btn-sm">Delete</a>';
                           return $btn;
                    })
                    ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        if($request->btnsave =="create-customer"){
        $validator = Validator::make(
            $request->all(),
            [
            'name'     => 'required|alpha',
            'mobile'     => 'required',
            'email'     => 'required',
            ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 406);
        }
        $profileImage ='';
        if ($files = $request->file('profile_image')) {
            // Define upload path
            $destinationPath = public_path('/profile_images/'); // upload path
         // Upload Orginal Image           
            $profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
            $files->move($destinationPath, $profileImage);
 
            
         }
        $admin = new Companies();
        $admin->name = $request->name;
        $admin->email = $request->email;
        $admin->mobile = $request->mobile;
        $admin->logo = $profileImage;
        $admin->save();
       
			$msg = 'Company data is created successfully';
		return redirect()->route('company.index')->with('success',$msg);
    }else{
        $affected = DB::table('companies')
              ->where('id', $request->cust_id)
              ->update(['name' => $request->name,'email' => $request->email,'mobile' => $request->mobile]);
              $msg = 'Company data is update successfully';
		return redirect()->route('company.index')->with('success',$msg);
    }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $where = array('id' => $id);
		$customer = Companies::where($where)->first();
		return response()->json([
            "data" => $customer,]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $exist = Employees::where('company_name',$id)->first();
        if(empty($exist)){
            Companies::find($id)->delete($id);
    
            return response()->json([
                'success' => 'Record deleted successfully!'
            ]);
        }
        return response()->json([
            'success' => 'already used!'
        ]);
    }
}
